# XServ contentApp

Repositorio plantilla para la entrega de la práctica "Clase contentApp"

# Enunciado

Esta clase, basada en el esquema de clases definido en el ejercicio "Clase servidor de aplicaciones", sirve el contenido almacenado en un diccionario Python. La clave del diccionario es el nombre de recurso a servir, y el valor es el cuerpo de la página HTML correspondiente a ese recurso.

# Forma de entrega

La entrega se realizará en un repositorio que se haya creado como fork de este.
